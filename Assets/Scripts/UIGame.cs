﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;//Te da acceso a todas las clases de Interface: buttones, Text, etc

namespace Main
{
    public class UIGame : MonoBehaviour
    {

        [SerializeField]
        Transform playerTransform;

        [SerializeField]
        Text scoreLabel;

        void Update()
        {
            scoreLabel.text = ( ( int ) playerTransform.position.z ) + " m";
        }
    }

}