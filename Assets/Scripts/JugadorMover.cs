﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{
    public class JugadorMover : MonoBehaviour
    {
        [SerializeField] GameManager gameManager;

        [SerializeField] Vector3 velocidad;

        Transform myTransform;
        Rigidbody rigid;

        float horizontal;

        // La primera funcion que se invoca
        void Start()
        {
            rigid = gameObject.GetComponent<Rigidbody>();
        }

        // Update se invoca una vez por frame
        void Update()
        {
            horizontal = Input.GetAxis( "Horizontal" );//-1 izq, +1 derecha

            //Debug.Log(horizontal);
        }

        //Este update se invoca cuando el sistema de fisica se actualiza
        void FixedUpdate()
        {
            rigid.velocity = new Vector3( horizontal * velocidad.x , 0 , velocidad.z );
        }

        //Esta funcion se invoca cuando mi rigidbody collisiona con otro objeto
        void OnCollisionEnter( Collision _collision )
        {
            if ( _collision.collider.gameObject.tag == "Obstaculo" )
            {
                gameManager.GameOver( ( int ) transform.position.z );
            }

            //Debug.Log( "Collision contra: "+ _collision.collider.name );
        }
    }

}