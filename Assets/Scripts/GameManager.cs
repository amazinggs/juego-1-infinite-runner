﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] GameObject gameoverPanel;

        void Awake()
        {
            Time.timeScale = 1f;
        }

        public void GameOver( int _score )
        {
            Time.timeScale = 0f;//congela el tiempo

            gameoverPanel.SetActive( true );//mostrar pantalla de gameover

            int bestScore = PlayerPrefs.GetInt("BEST_SCORE", 0);
            if(_score > bestScore)
                PlayerPrefs.SetInt( "BEST_SCORE" , _score );
        }

    }
}
