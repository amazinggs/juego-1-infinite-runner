﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Menu
{
    public class UIMenu : MonoBehaviour
    {
        [SerializeField]
        Text bestscoreLabel;

        void Start()
        {
            int bestScore = PlayerPrefs.GetInt("BEST_SCORE", 0);
            bestscoreLabel.text = "Best Score: "+ bestScore +" m";
        }
    }
}
