﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIButtonUtil : MonoBehaviour
{
    public void UI_LoadScene(string _sceneName)
    {
        SceneManager.LoadScene( _sceneName , LoadSceneMode.Single );
    }

    public void UI_AddScene( string _sceneName )
    {
        SceneManager.LoadScene( _sceneName , LoadSceneMode.Additive );
    }

    public void UI_UnloadScene( string _sceneName )
    {
        SceneManager.UnloadSceneAsync( _sceneName );
    }

}
