﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{
    public class Spawner : MonoBehaviour
    {
        [SerializeField] Transform playerTransform;

        [SerializeField] GameObject[] floorPrefabs;

        Vector3 lastPosition;//la posicion donde fue instanciado el ultimo floor

        void Start()
        {
            Spawn_Floor( floorPrefabs[ 0 ] , Vector3.zero );
        }

        void Update()
        {
            if ( playerTransform.position.z > 25f + lastPosition.z )
            {
                Spawn_RandomFloor( lastPosition + Vector3.forward * 100f );
            }
        }

        void Spawn_RandomFloor( Vector3 _position )
        {
            int r = Random.Range(0,floorPrefabs.Length);
            Spawn_Floor( floorPrefabs[ r ] , _position );
        }

        public void Spawn_Floor( GameObject _prefab , Vector3 _position )
        {
            var clone = Instantiate( _prefab , _position , Quaternion.identity );
            clone.transform.SetParent( transform );

            lastPosition = _position;
        }
    }

}